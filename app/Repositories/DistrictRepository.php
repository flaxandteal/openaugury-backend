<?php

namespace App\Repositories;

use App\Models\Features\District;
use InfyOm\Generator\Common\BaseRepository;
use A17\Twill\Repositories\ModuleRepository;

class DistrictRepository extends ModuleRepository
{

    public function __construct(District $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return District::class;
    }
}
