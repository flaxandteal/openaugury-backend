<?php

namespace App\Repositories;

use App\Models\Features\FeatureSet;
use InfyOm\Generator\Common\BaseRepository;

class FeatureSetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FeatureSet::class;
    }
}
