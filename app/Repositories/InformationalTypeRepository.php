<?php

namespace App\Repositories;

use App\Models\Interaction\InformationalType;
use InfyOm\Generator\Common\BaseRepository;

class InformationalTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'slug',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return InformationalType::class;
    }
}
