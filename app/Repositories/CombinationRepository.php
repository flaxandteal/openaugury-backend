<?php

namespace App\Repositories;

use App\Models\AbstractTier\Combination;
use InfyOm\Generator\Common\BaseRepository;

class CombinationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'status',
        'numerical_model_id',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Combination::class;
    }
}
