<?php

namespace App\Repositories;

use App\Models\Users\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identifier',
        'password',
        'affiliation_id',
        'teacher_id',
        'forename',
        'surname',
        'email',
        'remember_token'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
