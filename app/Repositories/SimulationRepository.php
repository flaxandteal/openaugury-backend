<?php

namespace App\Repositories;

use App\Models\SimulationTier\Simulation;
use InfyOm\Generator\Common\BaseRepository;
use A17\Twill\Repositories\ModuleRepository;

class SimulationRepository extends ModuleRepository
{

    public function __construct(Simulation $model)
    {
        $this->model = $model;
    }

    /**
     * @var array
     */

    protected $fieldSearchable = [
        'case_context_id',
        'combination_id',
        'settings',
        'definition',
        'status',
        'center',
        'extent',
        'begins',
        'ends',
        'result',
        'owner_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Simulation::class;
    }
}
