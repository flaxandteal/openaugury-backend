<?php

namespace App\Repositories;

use App\Models\Interaction\Informational;
use InfyOm\Generator\Common\BaseRepository;

class InformationalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'text',
        'subtext',
        'informational_type_id',
        'simulation_id',
        'creator_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Informational::class;
    }
}
