<?php namespace App\Helpers;

use Illuminate\Support\Collection;
use App\Models\CachedDataServerFeatureSet;

class OrpFeatureHelper
{
    public function pull($district, $center, $radius) {
        $dataSetFeatures = new Collection;
        $district->featureSets->each(function ($featureSet) use (&$dataSetFeatures, $center, $radius) {
            $cachedDataSet = CachedDataServerFeatureSet::whereDataServerSetId($featureSet->pivot->data_server_set_id)->whereDataServer($featureSet->pivot->data_server)->first();
            if ($cachedDataSet) {
                $features = $cachedDataSet
                    ->cachedDataServerFeatures()
                    ->whereRaw("ST_DWithin(location, ST_GeomFromText('" . $center->toWKT() . "'), " . $radius . ")");

                if ($features->count()) {
                    $dataSetFeatures[] = [
                        'featureSet' => $featureSet,
                        'features' => $features->get()
                    ];
                }
            }
        });

        return $dataSetFeatures;
    }
}
