<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\OrpFeatureHelper;
use App\Models\SimulationTier\Simulation;
use App\Models\CaseTier\CaseContext;
use App\Models\Interaction\Challenge;
use App\Models\Interaction\ChallengeTemplate;
use Phaza\LaravelPostgis\Geometries\Point;
use App\Models\CaseTier\CaseFeatureChunk;
use App\Models\Features\FeatureSet;
use Illuminate\Routing\UrlGenerator;
use Auth;
use Laravel\Cashier\Cashier;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(OrpFeatureHelper $featureHelper, UrlGenerator $urlGenerator)
    {
        if (!env('ALLOW_HTTP_URL', false)) {
            $urlGenerator->forceScheme('https');
        }

        Simulation::saving(function ($simulation) {
            if (Auth::user() && !Auth::user()->hasRole('admin')) {
                $simulation->owner_id = Auth::user()->id;
            }
        });

        Challenge::creating(function ($challenge) {
            $templateId = $challenge->challenge_template_id;
            if ($templateId) {
                $template = ChallengeTemplate::find($templateId);
                $challengeTemplateFields = [
                    'text',
                    'subtext',
                    'challenge_type',
                    'options',
                    'correct',
                    'mark'
                ];
                if ($template) {
                    foreach ($challengeTemplateFields as $field) {
                        if (!$challenge->getAttribute($field)) {
                            $challenge->setAttribute($field, $template->getAttribute($field));
                        }
                    }
                }
            }
        });

        CaseContext::saving(function ($simulation) {
            if (Auth::user() && !Auth::user()->hasRole('admin')) {
                $simulation->owner_id = Auth::user()->id;
            }
        });

        CaseContext::created(function ($caseContext) use ($featureHelper) {
            // At this point, we are in the middle of a performInsert,
            // so Phaza's PostgisTrait is currently storing the objects
            // in the geometries member, while the expressions are in the
            // respective attributes.
            if ($caseContext->district && isset($caseContext->geometries['center'])) {
                $featureSets = $featureHelper->pull(
                    $caseContext->district,
                    $caseContext->geometries['center'],
                    5000
                );

                $featureSets->each(function ($featureSet) use ($caseContext) {
                    $stats = json_encode([
                        'size' => $featureSet['features']->count()
                    ]);
                    $chunk = CaseFeatureChunk::create([
                        'case_context_id' => $caseContext->id,
                        'feature_set_id' => $featureSet['featureSet']->id,
                        'stats' => $stats,
                        'chunk' => json_encode($featureSet['features']->map(function ($f) {
                            return [
                                'feature_id' => $f->feature_id,
                                'location' => $f->location,
                                'extent' => $f->extent,
                                'data' => json_decode($f->json)
                            ];
                        })->toArray())
                    ]);
                });
            }
            $focalPoint = FeatureSet::whereSlug('focal-point')->first();
            if ($focalPoint) {
                CaseFeatureChunk::create([
                    'case_context_id' => $caseContext->id,
                    'feature_set_id' => $focalPoint->id,
                    'chunk' => json_encode([
                        [
                            'feature_id' => 1,
                            'location' => $caseContext->geometries['center']
                        ]
                    ])
                ]);
            }
        });

        \App\Models\SimulationTier\Simulation::saving(function ($simulation) {
            if (!$simulation->begins && !$simulation->ends) {
                $simulation->updateFromCase();
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
