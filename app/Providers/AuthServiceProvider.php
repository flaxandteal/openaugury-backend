<?php

namespace App\Providers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Models\SimulationTier\Simulation::class => \App\Policies\SimulationPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach ($this->policies as $module => $policy) {
            Gate::define($module, function ($user) use ($module) {
                return $user->can('viewAny', $module);
            });
        }

        /* These are replaced by per-module permissions */

        Gate::define('list', function ($user) {
            return true;
        });

        Gate::define('edit', function ($user) {
            return true;
        });
    }
}
