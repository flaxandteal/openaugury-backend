<?php

namespace App\Jobs;

use App\Models\SimulationTier\Simulation;
use App\Models\Users\User;

use App\Helpers\OrpSimulationHelper;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SimulationExecute implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $simulation;

    protected $simulator;

    protected $redis;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Simulation $simulation, User $simulator)
    {
        $this->simulation = $simulation;
        $this->simulator = $simulator;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(OrpSimulationHelper $helper)
    {
        if ($this->simulator && !$this->simulator->canSimulate()) {
            \Log::warning("Insufficient simulation credits for user " . $this->simulator->id);
            // TODO: improve notification for user
            return false;
        }

        $this->simulation->resultFeatureChunks()->delete();
        $this->simulation->featureArcs()->delete();
        $center = [
            $this->simulation->center->getLat(),
            $this->simulation->center->getLng()
        ];
        $window = [
            $this->simulation->center->getLat() - 0.08,
            $this->simulation->center->getLng() - 0.16,
            $this->simulation->center->getLat() + 0.08,
            $this->simulation->center->getLng() + 0.16,
        ];
        $risk90 = \App\Models\Features\FeatureSet::whereSlug('result-90-risk')->first();

        $caseFeatures = $this->simulation->caseContext->caseFeatureChunks->map(function ($caseFeatureChunk) {
            return [
                'set_id' => $caseFeatureChunk->id,
                'chunk' => json_decode($caseFeatureChunk->chunk)
            ];
        });

        $response = $helper->result(
            $this->simulation->id,
            $this->simulation->begins->timestamp,
            $this->simulation->ends->timestamp,
            json_encode($window),
            json_encode($center),
            $caseFeatures,
            $this->simulation->combination->numericalModel->phenomenon->slug,
            $this->simulation->combination->numericalModel->definition
        );

        if ($response && $response['results']) {
            \Log::info($response['results']);
            foreach ($response['results'] as $result) {
                $timeOffset = $result['time'] - $this->simulation->begins->timestamp;
                $resultFeatureChunk = \App\Models\SimulationTier\ResultFeatureChunk::create(
                    [
                        'simulation_id' => $this->simulation->id,
                        'feature_set_id' => $risk90->id,
                        'chunk' => json_encode(
                            [
                                'location' => [$this->simulation->center->getLat(), $this->simulation->center->getLng()],
                                'extent' => null,
                                'grid' => $result['grid']
                            ],
                            true
                        ),
                        'time_offset' => $timeOffset,
                    ]
                );
                $resultFeatureChunk['arc'] = json_encode($result['points'], true);
                $resultFeatureChunk->save();
            }

            foreach ($response['feature_arcs'] as $caseFeatureChunkId => $featureArc) {
                $featureArc = \App\Models\SimulationTier\FeatureArc::create([
                    'feature_chunk_id' => $caseFeatureChunkId,
                    'simulation_id' => $this->simulation->id,
                    'arc' => json_encode($featureArc)
                ]);
            }

            $this->simulation->status = Simulation::STATUS_COMPLETE;
            $this->simulation->save();

            if ($this->simulator) {
                $this->simulator->useSimulationCredit(1);
            }
        } else {
            $this->simulation->status = Simulation::STATUS_FAILED;
            $this->simulation->save();
        }
    }
}
