<?php

namespace App\Http\Controllers\SimulationTier;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;
use App\Models\Features\FeatureSet;
use App\Models\CaseTier\CaseFeatureChunk;
use App\Models\CaseTier\CaseContext;
use App\Helpers\OrpReportHelper;
use Illuminate\Http\Request;
use Response;
use App\Models\SimulationTier\Simulation;
use App\Jobs\SimulationRun;

class SimulationController extends CrudController
{

    public function __construct(\Lang $lang, Request $request)
    {
        parent::__construct($lang);

        $this->request = $request;
        $this->features['all']['import-button'] = false;
        $this->features['all']['export-button'] = false;
    }

    public function all($entity)
    {
        parent::all($entity);

        $entity = str_replace('\\', ':', $entity);

        $simulationModel = new \App\Models\SimulationTier\Simulation;
        $shared = false;
        if ($this->request->get('shared')) {
            $shared = true;
            $simulationModel = $simulationModel->whereShared(Simulation::SHARED_GLOBAL);
        } elseif (!Auth::user()->hasRole('admin')) {
            $simulationModel = $simulationModel->whereOwnerId(Auth::user()->id);
        }
        $simulationModel->with('combination.numericalModel.phenomenon', 'caseContext');

        if ($shared) {
            $title = "Simulations shared with me";
        } else {
            $title = "My Simulations";
        }

        // TODO: tidy up and use URL methods
        $addUrl = null;
        $caseContextId = null;
        $caseContextSuffix = '';
        if ($this->request->has('case_context_id')) {
            $caseContext = CaseContext::findOrFail($this->request->get('case_context_id'));
            $caseContextSuffix = '&case_context_id=' . $this->request->get('case_context_id');
            $addUrl = url('panel', $entity . '/edit') . '?case_context_id=' . $this->request->get('case_context_id');
            $title .= " for  \"" . $caseContext->name . "\" Target Zone";
            $simulationModel = $simulationModel->whereCaseContextId($caseContext->id);
            $this->filter = \DataFilter::source($simulationModel);

            $this->features['all']['return-button'] = [
                'target' => url('panel', 'CaseTier:CaseContext/edit') . '?modify=' . $caseContext->id,
                'label' => 'Edit Target Zone'
            ];
        } else {
            $this->filter = \DataFilter::source($simulationModel);
        }

        $this->grid = \DataGrid::source($this->filter);
        $modifyTarget = url('panel/' . $entity . '/edit?modify={{ $id }}');
        $column = $this->grid->add('generatedTitle', 'Title');
        if (!$shared) {
            $column->link($modifyTarget);
        }
        $caseTarget = url('panel', 'CaseTier:CaseContext/edit') . '?modify={{ $case_context_id }}';
        $column = $this->grid->add('{{ $caseContext->name }}', 'Target Zone');
        if (!$shared) {
            $column->link($caseTarget);
        }
        $this->grid->add('begins', 'From', true);
        $this->grid->add('ends', 'To', true);
        $this->grid->add('created_at', 'Created', true);
        $this->grid->add('updated_at', 'Updated', true);
        $this->grid->add('status', 'Status', 'text')->cell(function ($s) use ($caseContextSuffix) {
            $statusText = Simulation::$statusLabels[$s];
            if ($s == Simulation::STATUS_IN_PROGRESS) {
                $statusText .= " <a href=\"" . $this->request->fullUrl() . $caseContextSuffix . "\">[\u{21BB} Refresh]</a>";
            }
            return $statusText;
        });

        $this->grid->add_url = $addUrl;
        $downloadTarget = url('panel/SimulationTier:Simulation/downloadReport/{{ $id }}');
        // $downloadTarget = url('panel/' . $entity . '/downloadReport/f266f9ed-f835-4a3d-9123-08517269cac5');
        $this->grid->add('{{ "Report [as HTML]" }}', 'Download Report')->link($downloadTarget);

        $simulationStatuses = [Simulation::STATUS_INACTIVE, Simulation::STATUS_UNPROCESSED];
        $displayTarget = url('/#/s/{{ $id }}');
        // TRANSLATE
        $this->grid->add('{{ in_array($status, [' . implode(', ', $simulationStatuses) . ']) ? "" : "Display" }}', 'WEB LINK FOR DISTRIBUTION')->link($displayTarget);
        // TRANSLATE
        if ($shared) {
            $copyTarget = url('panel/' . $entity . '/edit?show={{ $id }}&copy=1');
            $this->grid->add('{{ "Copy" }}', "Make a copy for myself")->link($copyTarget);
        } else if (Auth::user()->canSimulate()) {
            $runTarget = url('panel/' . $entity . '/edit?show={{ $id }}&run=1') . $caseContextSuffix;
            $this->grid->add('{{ in_array($status, [' . implode(', ', $simulationStatuses) . ']) ? "Run" : "Re-run" }}', 'Run')->link($runTarget);
        } else if (Auth::user()->canSimulate() == 0) {
            $topupTarget = url('panel/edit');
            $this->grid->add('{{ "Top up" }}', 'Top Up')->link($topupTarget);
        }

        if (!$shared) {
            $this->addStylesToGrid();
        }

        $this->grid->orderBy('updated_at', 'desc');

        return $this->returnView('panelViews::all', ['title' => $title]);
    }

    public function downloadReport(OrpReportHelper $helper, $id)
    {
        $simulationModel = Simulation::findOrFail($id);
        $report = $helper->exportSimulationReportToTemporaryFile($simulationModel);
        return Response::download(
            $report,
            config('openaugury.brand.slug') . '-report.html',
            ['Content-Type' => 'text/html']
        )->deleteFileAfterSend(true);
    }

    public function edit($entity) {

        parent::edit($entity);

        $entity = str_replace('\\', ':', $entity);

        $user = Auth::user();

        $this->edit = \DataEdit::source(new Simulation);

        $indexUrl = url('panel/' . $entity . '/all');
        if ($this->request->get('case_context_id')) {
            $indexUrl .= '?case_context_id=' . $this->request->get('case_context_id');
        }

        if ($this->request->get('copy') && $this->edit->model->id) {
            if ($this->edit->model->shared == Simulation::SHARED_GLOBAL) {
                Simulation::takeACopy($user, $this->edit->model);
                return redirect($indexUrl);
            }
            abort(403);
        }

        // If the user is trying to edit an existing model, unless handled
        // above, they must own it
        if (!Auth::user()->hasRole('admin') && $this->edit->model->id && $this->edit->model->owner_id != $user->id) {
            abort(403);
        }

        if (!$this->edit->model->id && !$this->request->get('step')) {
            $url = url('panel', $entity . '/edit') . '?step=1';
            if ($this->request->get('case_context_id')) {
                $url .= '&case_context_id=' . $this->request->get('case_context_id');
            }
            return redirect($url);
        }

        // Catch an execution request
        if ($this->request->get('run')) {
            $simulation = Simulation::findOrFail($this->edit->model->id);
            dispatch(new SimulationRun($simulation, Auth::user(), True));
            return redirect($indexUrl);
        }

        $this->edit->label('Edit Simulation');

        $this->edit->add('combination_id', 'Phenomenon', 'select')->options(\App\Models\AbstractTier\Combination::all()->pluck('phenomenon_name', 'id'));

        $caseContext = $this->edit->add('case_context_id', 'Case', 'select')->options(\App\Models\CaseTier\CaseContext::pluck('name', 'id')->all());
        if ($this->request->has('case_context_id')) {
            $caseContext->insertValue($this->request->get('case_context_id'));
        }

        // RMV autofill
        $this->edit->add('settings', '', 'hidden')->insertValue('[]');
        $this->edit->add('definition', '', 'hidden')->insertValue('[]');
        $this->edit->add('status', '', 'hidden')->insertValue('0');
        $this->edit->add('result', '', 'hidden')->insertValue('[]');

        if ($this->edit->model->id) {
            $informationalTarget = url('panel/Interaction:Informational/all?search=1&simulation_id={{ $id }}');
            $this->edit->add('informationals', 'Informationals', 'container')
                ->content('<a href="' . $informationalTarget . '">Add to or edit informationals</a>');
            $challengeTarget = url('panel/Interaction:Challenge/all?search=1&simulation_id={{ $id }}');
            $this->edit->add('challenges', 'Tasks', 'container')
                ->content('<a href="' . $challengeTarget . '">Add to or edit tasks</a>');
        }

        $this->edit->saved(function () use ($entity) {
            if ($this->request->get('step') == '1') {
                return redirect(url('panel', $entity . '/edit') . '?step=2&modify=' . $this->edit->model->id);
            } else {
                return redirect(url('panel', $entity . '/all') . '?case_context_id=' . $this->edit->model->case_context_id);
            }
        });

        $this->edit->build();
        return $this->returnEditView('panelViews::editSimulation', ['form' => $this->edit, 'title' => 'Edit Simulation']);
    }
}
