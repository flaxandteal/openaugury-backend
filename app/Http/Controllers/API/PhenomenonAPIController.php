<?php

namespace App\Http\Controllers\API;

use App\Transformers\PhenomenonTransformer;
use App\Http\Requests\API\CreatePhenomenonAPIRequest;
use App\Http\Requests\API\UpdatePhenomenonAPIRequest;
use App\Models\AbstractTier\Phenomenon;
use App\Repositories\PhenomenonRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PhenomenonController
 * @package App\Http\Controllers\API
 */

class PhenomenonAPIController extends AppBaseController
{
    /** @var  PhenomenonRepository */
    private $phenomenonRepository;

    public function __construct(PhenomenonRepository $phenomenonRepo)
    {
        $this->phenomenonRepository = $phenomenonRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/phenomenons",
     *      summary="Get a listing of the Phenomenons.",
     *      tags={"Phenomenon"},
     *      description="Get all Phenomenons",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Phenomenon")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function index(Request $request)
    {
        // $this->phenomenonRepository->pushCriteria(new RequestCriteria($request));
        // $this->phenomenonRepository->pushCriteria(new LimitOffsetCriteria($request));
        $phenomenons = $this->phenomenonRepository->all();

        return fractal($phenomenons, new PhenomenonTransformer)
                ->withResourceName('phenomenons')->respond();
    }

    /**
     * @param CreatePhenomenonAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/phenomenons",
     *      summary="Store a newly created Phenomenon in storage",
     *      tags={"Phenomenon"},
     *      description="Store Phenomenon",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Phenomenon that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Phenomenon")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Phenomenon"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function store(CreatePhenomenonAPIRequest $request)
    {
        $input = $request->all();

        $phenomenon = $this->phenomenonRepository->create($input);

        return fractal($phenomenon, new PhenomenonTransformer)
                ->withResourceName('phenomenons')->respond();
    }

    /**
     * @param string $slug
     * @return Response
     *
     * @SWG\Get(
     *      path="/phenomenons/{slug}",
     *      summary="Display the specified Phenomenon",
     *      tags={"Phenomenon"},
     *      description="Get Phenomenon",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="slug",
     *          description="slug of Phenomenon",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Phenomenon"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function show($slug)
    {
        /** @var Phenomenon $phenomenon */
        $phenomenon = $this->phenomenonRepository->findBySlug($slug);

        if (empty($phenomenon)) {
            return $this->sendError('Phenomenon not found');
        }

        return fractal($phenomenon, new PhenomenonTransformer)
                ->withResourceName('phenomenons')->respond();
    }

    /**
     * @param string $slug
     * @param UpdatePhenomenonAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/phenomenons/{slug}",
     *      summary="Update the specified Phenomenon in storage",
     *      tags={"Phenomenon"},
     *      description="Update Phenomenon",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="slug",
     *          description="slug of Phenomenon",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Phenomenon that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Phenomenon")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Phenomenon"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function update($slug, UpdatePhenomenonAPIRequest $request)
    {
        $input = $request->all();

        /** @var Phenomenon $phenomenon */
        $phenomenon = $this->phenomenonRepository->findBySlug($slug);

        if (empty($phenomenon)) {
            return $this->sendError('Phenomenon not found');
        }

        $phenomenon = $this->phenomenonRepository->update($input, $phenomenon->id);

        return fractal($phenomenon, new PhenomenonTransformer)
                ->withResourceName('phenomenons')->respond();
    }

    /**
     * @param string $slug
     * @return Response
     *
     * @SWG\Delete(
     *      path="/phenomenons/{slug}",
     *      summary="Remove the specified Phenomenon from storage",
     *      tags={"Phenomenon"},
     *      description="Delete Phenomenon",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="slug",
     *          description="slug of Phenomenon",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */

    public function destroy($slug)
    {
        /** @var Phenomenon $phenomenon */
        $phenomenon = $this->phenomenonRepository->findBySlug($slug);

        if (empty($phenomenon)) {
            return $this->sendError('Phenomenon not found');
        }

        $this->phenomenonRepository->delete($phenomenon->id);

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
