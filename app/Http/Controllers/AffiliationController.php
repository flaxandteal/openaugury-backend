<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAffiliationRequest;
use App\Http\Requests\UpdateAffiliationRequest;
use App\Repositories\AffiliationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AffiliationController extends AppBaseController
{
    /** @var  AffiliationRepository */
    private $affiliationRepository;

    public function __construct(AffiliationRepository $affiliationRepo)
    {
        $this->affiliationRepository = $affiliationRepo;
    }

    /**
     * Show the form for creating a new Affiliation.
     *
     * @return Response
     */
    public function create()
    {
        return view('affiliations.create');
    }

    /**
     * Store a newly created Affiliation in storage.
     *
     * @param CreateAffiliationRequest $request
     *
     * @return Response
     */
    public function updateAddSimulations10($affiliationId)
    {
        $affiliation = $this->affiliationRepository->find($affiliationId);

        if ($affiliation) {
            $affiliation->addSimulationCredit(30);
            $affiliation->save();

            Flash::success('Affiliation updated successfully.');
        } else {
            // FIXME: should notify admin!
            \Log::error('Could not add credit to affiliation.');
            Flash::error('Could not add credit to affiliation.');
        }

        return redirect('/panel/edit');
    }
}
