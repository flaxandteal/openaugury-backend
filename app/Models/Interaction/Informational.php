<?php

namespace App\Models\Interaction;

use Eloquent as Model;

use Alsofronie\Uuid\UuidModelTrait;

/**
 * @SWG\Definition(
 *      definition="Informational",
 *      required={""},
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="informational_type_id",
 *          description="informational_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="time_offset",
 *          description="time offset",
 *          type="integer",
 *          format="int64"
 *      ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Informational extends Model
{

    use UuidModelTrait;

    public $table = 'informationals';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'text',
        'subtext',
        'informational_type_id',
        'time_offset',
        'simulation_id',
        'creator_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'text' => 'string',
        'subtext' => 'string',
        'informational_type_id' => 'integer',
        'time_offset' => 'integer'
    ];

    /**
     * Extra attributes that will be included in the final object
     *
     * @var integer
     */
    protected $appends = [
        'time'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * User who created this informational, if any
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function creator()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'creator_id', 'id');
    }

    /**
     * Get Simulation to which this applies
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }

    /**
     * Get type of informational that this is
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function informationalType()
    {
        return $this->belongsTo(\App\Models\Interaction\InformationalType::class, 'informational_type_id', 'id');
    }

    /**
     * Get the absolute time of this informational
     *
     * @return integer
     */
    public function getTimeAttribute()
    {
        if ($this->time_offset !== null && $this->simulation && $this->simulation->begins) {
            return (string) $this->simulation->begins->addSeconds($this->time_offset);
        }

        return null;
    }
}
