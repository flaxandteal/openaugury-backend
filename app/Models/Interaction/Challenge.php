<?php

namespace App\Models\Interaction;

use Eloquent as Model;

use Alsofronie\Uuid\UuidModelTrait;

/**
 * @SWG\Definition(
 *      definition="Challenge",
 *      required={""},
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="time_offset",
 *          description="time offset",
 *          type="integer",
 *          format="int64"
 *      ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="mark",
 *          description="mark",
 *          type="number",
 *          format="float"
 *      ),
 *      @SWG\Property(
 *          property="challenge_template_id",
 *          description="challenge_template_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="result_analytic_id",
 *          description="result_analytic_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Challenge extends Model
{

    use UuidModelTrait;

    public $table = 'challenges';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'text',
        'subtext',
        'time_offset',
        'options',
        'correct',
        'mark',
        'challenge_template_id',
        'simulation_id',
        'result_analytic_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'text' => 'string',
        'subtext' => 'string',
        'time_offset' => 'integer',
        'challenge_template_id' => 'integer',
        'result_analytic_id' => 'integer'
    ];

    /**
     * Attributes that should be appended when serializing
     *
     * @var array
     */
    protected $appends = [
        'time'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Answers that use this challenge
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function resultAnalytic()
    {
        return $this->belongsTo(\App\Models\SimulationTier\ResultAnalytic::class, 'result_analytic_id', 'id');
    }

    /**
     * Simulation to which this challenge belongs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }

    /**
     * Get the type of challenge this entity corresponds to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function challengeTemplate()
    {
        return $this->belongsTo(\App\Models\Interaction\ChallengeTemplate::class, 'challenge_template_id', 'id');
    }

    /**
     * Worksheet answers by individual students using this challenge
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function worksheetAnswers()
    {
        return $this->hasMany(\App\Models\Interaction\WorksheetAnswer::class);
    }

    /**
     * Get the absolute time of this informational
     *
     * @return integer
     */
    public function getTimeAttribute()
    {
        if ($this->simulation) {
            if ($this->time_offset !== null && $this->simulation->begins) {
                return (string) $this->simulation->begins->addSeconds($this->time_offset);
            }

            return (string) $this->simulation->begins;
        }

        return null;
    }

    /**
     * Reset the options field to something sensible
     */
    public function resetOptions()
    {
        $this->options = json_encode([
            0 => null,
            1 => null,
            2 => null,
            3 => null
        ]);
    }

    /**
     * Set the first option
     */
    public function setOption0Attribute($value)
    {
        if (!$this->options) {
            $this->resetOptions();
        }

        $options = json_decode($this->options, true);
        $options[0] = [
            'value' => 'a',
            'text' => $value
        ];
        $this->options = json_encode($options, true);
    }

    /**
     * Get the first option
     */
    public function getOption0Attribute()
    {
        $options = json_decode($this->options, true);
        if (is_array($options) && array_key_exists(0, $options) && array_key_exists('text', $options[2])) {
            return $options[0]['text'];
        }
        return null;
    }

    /**
     * Set the second option
     */
    public function setOption1Attribute($value)
    {
        if (!$this->options) {
            $this->resetOptions();
        }

        $options = json_decode($this->options, true);
        $options[1] = [
            'value' => 'b',
            'text' => $value
        ];
        $this->options = json_encode($options, true);
    }

    /**
     * Get the second option
     */
    public function getOption1Attribute()
    {
        $options = json_decode($this->options, true);
        if (is_array($options) && array_key_exists(1, $options) && array_key_exists('text', $options[2])) {
            return $options[1]['text'];
        }
        return null;
    }

    /**
     * Set the third option
     */
    public function setOption2Attribute($value)
    {
        if (!$this->options) {
            $this->resetOptions();
        }

        $options = json_decode($this->options, true);
        $options[2] = [
            'value' => 'c',
            'text' => $value
        ];
        $this->options = json_encode($options, true);
    }

    /**
     * Get the third option
     */
    public function getOption2Attribute()
    {
        $options = json_decode($this->options, true);
        if (is_array($options) && array_key_exists(2, $options) && array_key_exists('text', $options[2])) {
            return $options[2]['text'];
        }
        return null;
    }

    /**
     * Set the fourth option
     */
    public function setOption3Attribute($value)
    {
        if (!$this->options) {
            $this->resetOptions();
        }

        $options = json_decode($this->options, true);
        $options[3] = [
            'value' => 'd',
            'text' => $value
        ];
        $this->options = json_encode($options, true);
    }

    /**
     * Get the fourth option
     */
    public function getOption3Attribute()
    {
        $options = json_decode($this->options, true);
        if (is_array($options) && array_key_exists(3, $options) && array_key_exists('text', $options[2])) {
            return $options[3]['text'];
        }
        return null;
    }
}
