<?php

namespace App\Models\Users;

use Eloquent as Model;

use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Laravel\Cashier\Billable;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Users\User;
/**
 * @SWG\Definition(
 *      definition="Affiliation",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="district_id",
 *          description="district_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Affiliation extends Model
{
    use Sluggable;

    use Billable;

    public $table = 'affiliations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    const SIMULATIONS_UNMETERED = 0;
    const SIMULATIONS_METERED = 1;

    public $fillable = [
        'slug',
        'name',
        'district_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'district_id' => 'integer'
    ];

    /**
     * Commands to run on startup
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($affiliation) {
            $affiliation->code = Uuid::generate(4);
        });
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * This affiliation must belong to a district to identify potential datasets
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function district()
    {
        return $this->belongsTo(\App\Models\Features\District::class, 'district_id', 'id');
    }

    /**
     * Get the users within this Affiliation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function users()
    {
        return $this->hasMany(\App\Models\Users\User::class);
    }

    /**
     * Status constants for combinations wrt Affiliation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function combinations()
    {
        return $this->belongsToMany(\App\Models\AbstractTier\Combination::class, 'combination_affiliation')
            ->withPivot('status')
            ->withTimestamps();
    }

    /**
     * Add simulation credit if needs be
     *
     * @return integer Number of remaining simulations
     **/
    public function addSimulationCredit($number)
    {
        if ($this->isMetered()) {
            $this->simulations_remaining += $number;
        } else {
            $this->simulations_remaining = null;
        }

        return $this->simulations_remaining;
    }

    /**
     * Mark off a simulation credit if needs be
     *
     * @return integer Number of remaining simulations
     **/
    public function useSimulationCredit($number)
    {
        if ($this->isMetered()) {
            $this->simulations_remaining -= $number;

            if ($this->simulations_remaining < 0) {
                $this->simulations_remaining = 0;
            }

            return $this->simulations_remaining;
        }

        return null;
    }

    /**
     * Check for simulations active and in credit
     *
     * @return integer Number of remaining simulations
     **/
    public function canSimulate()
    {
        if ($this->isMetered()) {
            return (integer)$this->simulations_remaining;
        }

        return true;
    }

    /**
     * Set this model to be metered
     *
     * @param integer Initial number of remaining simulations
     **/
    public function setMetered(int $initialCount)
    {
        $this->simulations_status = self::SIMULATIONS_METERED;
        $this->simulations_remaining = $initialCount;
    }

    /**
     * Check if this model is metered
     *
     * @return bool Whether this model is metered
     **/
    public function isMetered()
    {
        return $this->simulations_status == self::SIMULATIONS_METERED;
    }
}
