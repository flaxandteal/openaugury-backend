<?php

namespace App\Models\AbstractTier;

use Eloquent as Model;

use Alsofronie\Uuid\UuidModelTrait;

/**
 * @SWG\Definition(
 *      definition="Combination",
 *      required={""},
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Combination extends Model
{

    use UuidModelTrait;

    public $table = 'combinations';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Values of the status property
     *
     * @var integer
     */
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_PRIVATE = 2;

    public $fillable = [
        'status',
        'numerical_model_id',
        'owner_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Owner of this object
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function owner()
    {
        return $this->belongsTo(\App\Models\Users\User::class, 'owner_id', 'id');
    }

    /**
     * For which numerical model is this combination?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function numericalModel()
    {
        return $this->belongsTo(\App\Models\AbstractTier\NumericalModel::class, 'numerical_model_id', 'id');
    }

    /**
     * Which simulations build on this combination?
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function simulations()
    {
        return $this->hasMany(\App\Models\SimulationTier\Simulation::class);
    }

    /**
     * Get the affiliations that are able to use this combination
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function affiliations()
    {
        return $this->belongsToMany(\App\Models\Users\Affiliation::class, 'combination_affiliation');
    }


    /**
     * Get the name of the phenomenon for this case
     *
     * @return string
     **/
    public function getPhenomenonNameAttribute()
    {
        if ($this->numericalModel) {
            return $this->numericalModel->descriptiveName;
        }

        return '(unknown)';
    }

    /**
     * Can this object be used?
     *
     * @return bool
     **/
    public function canIUse()
    {
        return $this->status === self::STATUS_ACTIVE || ($this->status === self::STATUS_PRIVATE && app()->user() == $this->owner);
    }
}
