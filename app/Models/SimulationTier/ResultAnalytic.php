<?php

namespace App\Models\SimulationTier;

use Eloquent as Model;


/**
 * @SWG\Definition(
 *      definition="ResultAnalytic",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="analytic_id",
 *          description="analytic_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ResultAnalytic extends Model
{


    public $table = 'result_analytics';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'analytic_id',
        'simulation_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'analytic_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * Simulation that created this resource requirement
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }

    /**
     * Analytic that this result provides
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function analytic()
    {
        return $this->belongsTo(\App\Models\AbstractTier\Analytic::class, 'analytic_id', 'id');
    }

    /**
     * Challenges that this answers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function challenges()
    {
        return $this->hasMany(\App\Models\Interaction\Challenge::class);
    }
}
