<?php

namespace App\Models\SimulationTier;

use Eloquent as Model;


class ResultFeatureChunk extends Model
{


    public $table = 'result_feature_chunks';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'chunk',
				'arc',
        'simulation_id',
        'feature_set_id',
        'time_offset'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'feature_set_id' => 'integer',
	'arc' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * FeatureSet described by this chunk
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function featureSet()
    {
        return $this->belongsTo(\App\Models\Features\FeatureSet::class, 'feature_set_id', 'id');
    }

    /**
     * Simulation that created this chunk
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function simulation()
    {
        return $this->belongsTo(\App\Models\SimulationTier\Simulation::class, 'simulation_id', 'id');
    }
}
