<?php

namespace App\Transformers;

use App\Models\Interaction\Informational;
use App\Transformers\InformationalTypeTransformer;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="Informational",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="text",
 *          description="text",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="subtext",
 *          description="subtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="time",
 *          description="time",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="time_offset",
 *          description="time_offset",
 *          type="integer",
 *          format="int64"
 *      ),
 *      @SWG\Property(
 *          property="informationalType",
 *          description="type of informational",
 *          type="object",
 *          @SWG\Item("InformationalType")
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class InformationalTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
            'informationalType',
    ];

    public function transform(Informational $informational)
    {
        $informationalArray = $informational->toArray();

        return [
            'id' => $informationalArray['id'],
            'text' => $informationalArray['text'],
            'subtext' => $informationalArray['subtext'],
            'time' => $informationalArray['time'],
            'time_offset' => $informationalArray['time_offset'],
            'created_at' => $informationalArray['created_at'],
            'updated_at' => $informationalArray['updated_at'],
        ];
    }

    public function includeInformationalType(Informational $informational)
    {
        $informationalType = $informational->informationalType;

        return $this->item($informationalType, new InformationalTypeTransformer, 'informationalTypes');
    }
}
