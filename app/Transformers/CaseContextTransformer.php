<?php

namespace App\Transformers;

use App\Models\CaseTier\CaseContext;
use App\Models\Features\District;
use League\Fractal;

/**
 * @SWG\Definition(
 *      definition="CaseContext",
 *      required={""},
 *      @SWG\Property(
 *          property="district",
 *          description="district",
 *          type="object",
 *          @SWG\Item("District")
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="center",
 *          description="center",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="extent",
 *          description="extent",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="begins",
 *          description="begins",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="ends",
 *          description="ends",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="owner_id",
 *          description="Person with primary responsibility for this case",
 *          type="string",
 *          format="uuid"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CaseContextTransformer extends Fractal\TransformerAbstract
{
    protected $defaultIncludes = [
            'district',
            'features'
    ];

    protected $availableIncludes = [
            'featureSets'
    ];

    public function transform(CaseContext $caseContext)
    {
        $caseContextArray = $caseContext->toArray();

        return [
            'id' => $caseContextArray['id'],
            'name' => $caseContextArray['name'],
            'center' => $caseContextArray['center'],
            'extent' => $caseContextArray['extent'],
            'district_id' => $caseContextArray['district_id'],
            'begins' => $caseContextArray['begins'],
            'ends' => $caseContextArray['ends'],
            'owner_id' => $caseContextArray['owner_id'],
            'created_at' => $caseContextArray['created_at'],
            'updated_at' => $caseContextArray['updated_at']
        ];
    }

    public function includeDistrict(CaseContext $caseContext)
    {
        $district = $caseContext->district;

        return $this->item($district, new DistrictTransformer, 'districts');
    }

    public function includeFeatures(CaseContext $caseContext)
    {
        $features = $caseContext->activeFeatureChunks;

        return $this->collection($features, new CaseFeatureChunkTransformer, 'caseFeatureChunks');
    }

    public function includeFeatureSets(CaseContext $caseContext)
    {
        $featureSets = $caseContext->featureSets;

        return $this->collection($featureSets, new FeatureSetTransformer, 'featureSets');
    }
}
