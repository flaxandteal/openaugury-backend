<?php

namespace App\Policies;

use App\Models\Users\User;
use App\Models\SimulationTier\Simulation;
use Illuminate\Auth\Access\HandlesAuthorization;

class SimulationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any simulations.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the simulation.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Simulation  $simulation
     * @return mixed
     */
    public function view(User $user, Simulation $simulation)
    {
        return true;
    }

    /**
     * Determine whether the user can create simulations.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the simulation.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Simulation  $simulation
     * @return mixed
     */
    public function update(User $user, Simulation $simulation)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the simulation.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Simulation  $simulation
     * @return mixed
     */
    public function delete(User $user, Simulation $simulation)
    {
        return true;
    }

    /**
     * Determine whether the user can restore the simulation.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Simulation  $simulation
     * @return mixed
     */
    public function restore(User $user, Simulation $simulation)
    {
        return true;
    }

    /**
     * Determine whether the user can permanently delete the simulation.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Simulation  $simulation
     * @return mixed
     */
    public function forceDelete(User $user, Simulation $simulation)
    {
        return true;
    }
}
