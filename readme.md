# openaugury-backend

> Back-end infrastructure for the OpenAugury project

# OpenAugury

# Licensing

This repository is AGPL - please note that this means that, unless agreed otherwise, reusing ANY code or serving all or part of this software to users is highly likely to mean you have to make your final app fully open source, including any simulation/backend functions (your additions can be under any AGPL-compatible open source license, from MIT to AGPL).

## What does this cover?

At present and by default, this covers any use of this software with your own simulation functions - *if you give access to anyone else, you are legally required to give them unresetricted access to any modifications and the combined work, _including any simulation function code_*. This also covers self-hosting any AGPL components for others, with or without a web interface.

*Of course, if you do not give access to anyone else, to this software or your combined work, you are not required to publicly release any changes* - but you must not put confidentiality or sharing restrictions (beyond standard open source license requirements) on anyone else, including on the simulation function code they have used via this software, which they have a right to access. This includes employees within the same organisation as well as third parties.

If you have ANY reservations about this, you MUST NOT use this software.

Please note that, at present, Flax &amp; Teal Limited is the sole holder of all AGPL code in this code-base and, as such, is entitled to agree separate commercial or charitable terms on a case-by-case basis. If you have a use-case that you feel is compatible with the open source principles of transparency but not the AGPL license used here, please get in touch.

## What if I want to contribute?

We do welcome contributions to the project - we do not believe Contributor License Agreements (CLAs) should be necessary for a range of reasons, but to ensure we are able to maintain our commercial work, we do require contributions to be under permissive licenses. Any merge requests to this repository will be taken to be under MIT license, unless stated otherwise. When merging, we effectively create a derived work based on your MIT-licensed changes under F&amp;T's AGPL license, but your MIT rights to your own code in the original merge request are unaffected. Acknowledgement and a link to your changes will be provided in our ACKNOWLEDGEMENTS file.

## But that's not fair!

While avoiding CLAs solves a number of key objections, one common concern that remains is that the primary copyright holder is allowed to sell commercial licenses to third-parties that contains contributed code (via merge requests), but other contributors cannot. This is still the case here - contributed code from anyone other than Flax &amp; Teal must be under permissive licenses, allowing us to sell the right to create non-open-source derivatives to our customers, while other contributors may only use or re-sell this application with all new code in the open.

If you are solely providing open source services to clients who are willing to consume fully open source services, these restrictions will not affect you.

If you wish to provide services, where your clients cannot see simulation function code, or other modifications, or contain closed-source freemium components, you will be in violation of the AGPL licensing agreement, unless you have obtained a license to do so from Flax &amp; Teal. Similarly, if your clients do so, they will also be in violation of the terms.

However, Flax &amp; Teal as a matter of company policy is not selling closed sourced or freemium content - we deliver open source software, and what you see here is what we sell. Any additional work that is not open is exclusively owned by and proprietary to our clients, even if commissioned from us - we are not selling any closed additions either.

## Doesn't open source mean all users should be unrestricted? AGPL restricts the projects other contributing developers...

We believe the objective of open source is to reduce proprietary restrictions to the ultimate users of a piece of software, and we believe this can necessitate incorporating open source restrictions, as here.

We usually license our libraries permissively (in contrast to this work) as the target users are developers. However, software such as this, where our target is non-technical end-users, uses strong licensing to ensure that our work drives the ecosystem to more transparency, and that end-users keep that freedom, at the cost of restrictions to proprietary development.

While Flax &amp; Teal does produce paid-for, client-owned proprietary code, we do so to make provide a net benefit to the open source ecosystem, (at least as long as our main commercial competitors are proprietary), by aiming for the best offering in the sector to be open source, by helping us avoid a freemium two-tier model, by funding non-volunteer requirements (such as security, compliance and penetration testing) and by ensuring essential project improvement timelines are not dependent on volunteer interest. Further, to avoid this becoming a net negative, we put contractual requirements in place to retain the open source focus of this project.

## I just want to try it out in the office and see if it works, doesn't this stop me?

To reiterate, if you are only providing this as a service to yourself, there are no relevant restrictions. If you want to get a few folks to try out your self-hosted version, and it hasn't materially changed from ours, then there's still no relevant restrictions. By that point, hopefully you will know if it's for you!

## I am a large enterprise that wants to wrap it up and offer it to my customers, how does this affect me?

Any changes you make will need to be released - however, more than that, whether you are running it live or batched, we explicitly consider the offering of an automated service that obtains results and returns them to a user to be a combined work. As an example, if a large cloud provider had this as a module that linked in, they would be required to open source at least parts of their user interface, their glue-code, their scheduling systems and any attendant infrastructure (so don't expect to see this on the big cloud providers, unless they've a commercial license).


## Will this change?

As this software gains momentum and the risk, investment and creative input ceases to be predominantly from Flax &amp; Teal, we plan to share our AGPL rights. We will do this by sharing our AGPL rights with a Contributors Trust, which will be able to decide which other contributors can create proprietary services or derived works and on what terms.

From an open source perspective, our ideal is that one day we will have a world where all software is unrestricted by secrecy or copyright to all users - however, in the current environment, this approach is the only realistic way to avoid building software that primarily benefits a proprietary competitor, thereby becoming a tool for restricting end-users' visibility into what their providers are doing. From a business perspective, ultimately sharing the rights in a Contributors Trust will allow the community to grow beyond one company, and ensures that the community can be financially sustainable independently of us - the only way an open source project, and so ourselves as open source providers, can succeed.

## I want to build or commission simulations or changes to this software

If you are happy for changes and simulation functions to be publicly shared, you are welcome to do so yourself, or to pay any other individual or organisation - you must make them aware of these terms, even if they are within your organisation.

If you are solely using providing access to yourself personally (commercially or non-commercially) - perhaps selling static results made using this software, or testing hypotheses locally - you are free to do so without further restriction.

If you are serving the software or parts of it to others, and not solely using the code for yourself (commercially or non-commercially), you must procure a non-AGPL license from Flax &amp; Teal Limited - this may or may not incur a cost.

## Does this affect the license of data created with this software?

No, as long as you do not share or serve the software (or portions of it) any data output is not considered part of a derived work - such as reports, static JSON created with our APIs, and so forth, is entirely your own.

# Note

YOUR USE OF THIS SOFTWARE OR RE-USE OF CODE HEREIN INDICATES LEGALLY-BINDING AGREEMENT WITH THE ABOVE TERMS.

For the remainder of the terms, please see the AGPLv3 license in the LICENSE file.

Any code not explicitly marked otherwise should be considered to be property of Flax &amp; Teal, licensed under the terms of the GNU AGPLv3 license. Certain other code is marked as under 3rd party licenses, which may not be AGPL.
