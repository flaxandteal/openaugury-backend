<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(FeatureSetsTableSeeder::class);
        $this->call(ChallengeTemplatesTableSeeder::class);
        $this->call(InformationalTypesTableSeeder::class);
        $this->call(PhenomenonsTableSeeder::class);

        if (App::environment('local', 'development', 'staging')) {
            $this->call(SampleUsersTableSeeder::class);
            $this->call(OpenDataSeeder::class);
            //$this->call(SimulationsTableSeeder::class);
        }
    }
}
