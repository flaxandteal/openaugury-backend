<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class ActivePlacesNISeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $data_source = CachedDataServerFeatureSet::whereName('Active Places NI')->first();

      if ($data_source)
          $data_source->cachedDataServerFeatures()->delete();
      else
          CachedDataServerFeatureSet::create([
            'name' => 'Active Places NI',
            'owner' => 'Sport NI',
            'license_title' => 'UK-OGL',
            'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
            'uri' => 'https://www.opendatani.gov.uk/dataset/active-places-ni-sports-facilities-database/resource/9615b5b6-3f11-4968-b337-f1dc1e9db828',
            'data_server' => 'opendatani',
            'data_server_set_id' => '9615b5b6-3f11-4968-b337-f1dc1e9db828'
          ]);

      $data_source = CachedDataServerFeatureSet::whereName('Active Places NI')->first();

      $district = District::whereName('Northern Ireland 1')->first();
      $feature_type = FeatureSet::whereName('Sports Facility')->first();
      $feature_type->districts()->sync([
          $district->id => [
              'data_server_set_id' => $data_source->data_server_set_id,
              'data_server' => 'opendatani',
              'status' => 0
          ]
      ]);


      $actplas = json_decode(file_get_contents(base_path() . '/resources/opendata/active-places-ni-converted.json'), true);
      $slugify = new Slugify();
      $n = 0;
      foreach ($actplas as $place) {
          $n++;
          $feature = new CachedDataServerFeature;
          $feature->feature_id = $n;
          $feature->location = new Point($place['lat'], $place['lng']);
          $feature->json = json_encode($place);
          $feature->cached_data_server_feature_set_id = $data_source->id;
          $feature->save();
      }
    }
}
