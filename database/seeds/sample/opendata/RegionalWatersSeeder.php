<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class RegionalWatersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $dataSource = CachedDataServerFeatureSet::whereName('Regional Waters')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            $dataSource = CachedDataServerFeatureSet::create([
			          'name' => 'Regional Waters',
				        'owner' => 'Department for Infrastructure - Roads',
					      'license_title' => 'UK-OGL',
						    'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
							  'uri' => 'https://www.opendatani.gov.uk/dataset/gsni-tellus-regional-stream-waters-icp',
								'data_server' => 'opendatani',
								'data_server_set_id' => 'regional-waters'
            ]);
        }

        $district = District::whereName('Northern Ireland 1')->first();
        $featureType = FeatureSet::whereSlug('regional-waters')->first();
        $featureType->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $regionalWatersJson = json_decode(file_get_contents(base_path() . '/resources/opendata/regionalwatersicp.geojson'));

        $regionalWaters = GeoJson::jsonUnserialize($regionalWatersJson);
        foreach ($regionalWaters as $regionalWatersItem) {
            $coordinates = $regionalWatersItem->getGeometry()->getCoordinates();
            $feature = new CachedDataServerFeature;
            $properties = $regionalWatersItem->getProperties();
            $feature->feature_id = $properties['SAMPLENO'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $feature->json = json_encode($regionalWatersItem);
            $feature->cached_data_server_feature_set_id = $dataSource->id;

            $feature->save();
        }
    }
}
