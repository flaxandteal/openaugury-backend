<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;

class TranslinkRailwayStationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data_source = CachedDataServerFeatureSet::whereName('Translink Railway Stations and Halts')->first();

        if ($data_source) {
            $data_source->cachedDataServerFeatures()->delete();
        } else {
            CachedDataServerFeatureSet::create([
              'name' => 'Translink Railway Stations and Halts',
              'owner' => 'TranslinkNI',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/northern-ireland-railways-stations',
              'data_server' => 'opendatani',
              'data_server_set_id' => '00000000-0000-0000-0000-000000000010'
            ]);
        }

        $data_source = CachedDataServerFeatureSet::whereName('Translink Railway Stations and Halts')->first();

        $district = District::whereName('Northern Ireland 1')->first();
        $feature_type = FeatureSet::whereName('Railway Station')->first();
        $feature_type->districts()->sync([
            $district->id => [
                'data_server_set_id' => $data_source->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $stationsJson = json_decode(file_get_contents(base_path() . '/resources/opendata/translink-nir-rail-stations.geojson'), true);

        $stations = GeoJson::jsonUnserialize($stationsJson);
        foreach ($stations as $station) {
            $coordinates = $station->getGeometry()->getCoordinates();
            $properties = $station->getProperties();

            $feature = new CachedDataServerFeature;
            $feature->feature_id = $properties['ID'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $feature->json = json_encode($station);
            $feature->cached_data_server_feature_set_id = $data_source->id;
            $feature->save();
        }

        $haltsJson = json_decode(file_get_contents(base_path() . '/resources/opendata/translink-nir-halts.geojson'), true);

        $halts = GeoJson::jsonUnserialize($haltsJson);
        foreach ($halts as $halt) {
            $coordinates = $halt->getGeometry()->getCoordinates();
            $properties = $halt->getProperties();

            $feature = new CachedDataServerFeature;
            $feature->feature_id = $properties['ID'];
            $feature->location = new Point($coordinates[1], $coordinates[0]);
            $feature->json = json_encode($halt);
            $feature->cached_data_server_feature_set_id = $data_source->id;
            $feature->save();
        }
    }
}
