<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use GeoJson\GeoJson;
use Illuminate\Database\Seeder;
use Cocur\Slugify\Slugify;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\Features\FeatureSet;
use App\Models\Features\District;
use League\Csv\Reader;

class NISchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $dataSource = CachedDataServerFeatureSet::whereName('NI Schools')->first();

        if ($dataSource) {
            $dataSource->cachedDataServerFeatures()->delete();
        } else {
            CachedDataServerFeatureSet::create([
              'name' => 'NI Schools',
              'owner' => 'Department of Education (NI)',
              'license_title' => 'UK-OGL',
              'license_url' => 'http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/',
              'uri' => 'https://www.opendatani.gov.uk/dataset/locate-a-school/resource/d0947faf-5d84-4ce4-80dd-ce4fa0e1c0d5',
              'data_server' => 'opendatani',
              'data_server_set_id' => '00000000-0000-0000-0000-000000000002'
            ]);
        }

        $dataSource = CachedDataServerFeatureSet::whereName('NI Schools')->first();

        $district = District::whereName('Northern Ireland 1')->first();
        $feature_type = FeatureSet::whereName('School Locations')->first();
        $feature_type->districts()->sync([
            $district->id => [
                'data_server_set_id' => $dataSource->data_server_set_id,
                'data_server' => 'opendatani',
                'status' => 0
            ]
        ]);

        $schoolsCsv = Reader::createFromPath(base_path() . '/resources/opendata/locate-a-school-open-data-feb-2016.csv');
        $headerRow = $schoolsCsv->fetchOne();
        $schoolsCsv->setOffset(1);
        $schoolsCsv->each(function ($row) use ($dataSource, $headerRow) {
            $feature = new CachedDataServerFeature;
            $feature->feature_id = $row[0];
            $feature->location = new Point($row[12], $row[13]);
            $feature->json = json_encode(array_combine($headerRow, $row));
            $feature->cached_data_server_feature_set_id = $dataSource->id;
            $feature->save();
            return true;
        });
    }
}
