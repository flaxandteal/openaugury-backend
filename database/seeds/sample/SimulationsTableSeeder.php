<?php

use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Seeder;
use App\Models\CachedDataServerFeature;
use App\Models\CachedDataServerFeatureSet;
use App\Models\CaseTier\CaseContext;
use App\Models\Interaction\Informational;
use App\Models\Interaction\InformationalType;
use App\Models\Interaction\Challenge;
use App\Models\Interaction\ChallengeTemplate;
use App\Models\CaseTier\CaseFeatureChunk;
use App\Models\SimulationTier\Simulation;
use App\Models\SimulationTier\ResultFeatureChunk;
use App\Models\SimulationTier\FeatureArc;
use App\Models\AbstractTier\Phenomenon;
use App\Models\Users\User;
use App\Models\Features\District;
use App\Models\Features\FeatureSet;

class SimulationsTableSeeder extends Seeder
{
  public function gridResultFromSimulation($simulation) {
    $center = [$simulation->center->getLat(), $simulation->center->getLng()];
    $risk90 = FeatureSet::whereSlug('result-90-risk')->first();

    $northWest = [$center[0] - 0.1, $center[1] - 0.1];
    $southEast = [$center[0] + 0.1, $center[1] + 0.1];
    $divisionsLat = 400;
    $divisionsLon = 400;

    $kmax = 10;
    for ($k = 1 ; $k < $kmax ; $k++) {
      $resultFeature = ResultFeatureChunk::create(
        [
          'simulation_id' => $simulation->id,
          'feature_set_id' => $risk90->id,
          'chunk' => json_encode(
            [
              'location' => $center,
              'extent' => null,
              'grid' => [
                'northWest' => $northWest,
                'southEast' => $southEast,
                'divisionsLat' => $divisionsLat,
                'divisionsLon' => $divisionsLon,
              ]
            ], true
          ),
          'time_offset' => $k * 3600
        ]
      );

      $values = [];
      for ($j = 1 ; $j < $divisionsLat ; $j++) {
        for ($i = 0 ; $i < $divisionsLon ; $i++) {
          $loc = [
            $northWest[0] + $j * ($southEast[0] - $northWest[0]) / $divisionsLat,
            $northWest[1] + $i * ($southEast[1] - $northWest[1]) / $divisionsLon,
          ];
          $dist2 = 4 * ($loc[0] - $center[0]) ** 2 + ($loc[1] - $center[1]) ** 2;
          if ($dist2 < 0.0001 * $k) {
            $values[$j . ',' . $i] = 1 + ($k / $kmax) * (max(min($dist2 * 1000, 1.0), 0.0) - 1);
          }
        }
      }

      $resultFeature->arc = json_encode($values, true);
      $resultFeature->save();
    }
  }

  public function randomExpandingResultFromSimulation($simulation) {
    $center = [$simulation->center->getLat(), $simulation->center->getLng()];
    $risk90 = FeatureSet::whereSlug('result-90-risk')->first();

    $points = [[0, []]];
    for ($i = 0 ; $i < 12 ; $i++) {
      $points[0][1][] = $center;
      $lineString[] = new Point($center[0], $center[1]);
    }
    $risk90Extent = new Polygon([new LineString($lineString)]);

    $resultFeature = ResultFeatureChunk::create(
      [
        'simulation_id' => $simulation->id,
        'feature_set_id' => $risk90->id,
        'chunk' => json_encode(
          [
            [
              'location' => $center//,
              //'extent' => $risk90Extent
            ]
          ]
        )
      ]
    );

    for ($j = 1 ; $j < 5 ; $j++) {
      $points[$j] = [$j * 3600, []];
      for ($i = 0 ; $i < 12 ; $i++) {
        $deg = $i * M_PI / 6;
        $prev = $points[$j - 1][1][$i];
        $raddel = rand(30, 50) / (50. * 100);
        $points[$j][1][] =  [$prev[0] + $raddel * cos($deg), $prev[1] + $raddel * sin($deg)];
      }
    }

    $resultFeature->arc = json_encode([$points], true);
    $resultFeature->save();
    }

    public function attachNearbyFeaturesToSimulation($simulation) {
      CachedDataServerFeatureSet::all()->each(function ($cachedDataServerFeatureSet) use ($simulation) {
        $features = CachedDataServerFeature::whereRaw("ST_DWithin(location, ST_GeomFromText('" . $simulation->center->toWKT() . "'), 5000)")->whereCachedDataServerFeatureSetId($cachedDataServerFeatureSet->id);
        $featureSetId = DB::table('district_feature_set')->where('data_server_set_id', '=', $cachedDataServerFeatureSet->data_server_set_id)->first()->feature_set_id;

        if ($features->count()) {
          $district = $simulation->caseContext->district;
          $features = $features->get();
          $chunk = CaseFeatureChunk::create([
            'case_context_id' => $simulation->case_context_id,
            'feature_set_id' => $featureSetId,
            'status' => CaseFeatureChunk::STATUS_ACTIVE,
            'chunk' => json_encode($features->map(function ($f) {
              return [
                'feature_id' => $f->feature_id,
                'location' => $f->location,
                //'extent' => $f->extent,
                'data' => json_decode($f->json)
              ];
            })->toArray())
          ]);
          FeatureArc::create([
            'simulation_id' => $simulation->id,
            'feature_chunk_id' => $chunk->id,
            'arc' => json_encode($features->map(function ($f) {
              return [
                'h' => [[0, 100], [3600, 90], [18000, 20], [36000, 0]]
              ];
            })->toArray(), true)
          ]);
        }
      });
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('simulations')->delete();
      DB::table('case_contexts')->delete();

      $simulation_location = new Point(54.21, -5.89);
      $simulation_extent = null;
      $simulation_extent = new Polygon([new LineString([
        new Point(54.101, -6.254),
        new Point(54.101, -6.252),
        new Point(54.103, -6.252),
        new Point(54.103, -6.254),
        new Point(54.101, -6.254),
      ])]);

      $teacher = User::whereEmail('admin@example.com')->first();
      $district = District::whereName('Northern Ireland 1')->first();
      $case = CaseContext::create([
        'name' => "near Newcastle",
        'center' => $simulation_location,
        //'extent' => $simulation_extent,
        'begins' => \Carbon\Carbon::now(),
        'ends' => \Carbon\Carbon::now()->addHours(10),
        'owner_id' => $teacher->id,
        'district_id' => $district->id
      ]);

      $combination = Phenomenon::whereName('Hurricane Storm Surge')->first()->numericalModels()->first()->combinations()->first();
      $simulation = Simulation::create([
        'status' => Simulation::STATUS_INACTIVE,
        'shared' => Simulation::SHARED_GLOBAL,
        'center' => $simulation_location,
        'extent' => $simulation_extent,
        'case_context_id' => $case->id,
        'combination_id' => $combination->id,
        'begins' => $case->begins,
        'ends' => $case->ends,
        'result' => '{}',
        'owner_id' => $teacher->id,
        'settings' => '{}',
        'definition' => '[DEFINITION]',
      ]);

      $newsFlash = InformationalType::whereSlug('newsflash')->first();
      $socialMedia = InformationalType::whereSlug('social-media')->first();
      $information = InformationalType::whereSlug('information')->first();

      $informational = Informational::create([
        'text' => 'Something happened',
        'subtext' => 'Dramatic event occurred',
        'time_offset' => 12000,
        'simulation_id' => $simulation->id,
        'informational_type_id' => $newsFlash->id
      ]);

      $informational = new Informational;
      $informational->text = "Flood Warning!";
      $informational->time_offset = 0;
      $informational->subtext = "Water levels predicted to rise steeply in the coming hours";
      $informational->simulation()->associate($simulation);
      $informational->informational_type_id = $information->id;
      $informational->save();

      $informational = new Informational;
      $informational->text = "Trending Social Media Post";
      $informational->informational_type_id = $socialMedia->id;
      $informational->author = "@boatergirl2017";
      $informational->time_offset = 3814;
      $informational->subtext = "What's up with the tide?? Why's my car underwater?! #waterworld";
      $informational->metadata = json_encode(['likes' => 251, 'shares' => 19]);
      $informational->simulation()->associate($simulation);
      $informational->save();

      $informational = new Informational;
      $informational->text = "News Channel Breaking Headline";
      $informational->time_offset = 5357;
      $informational->informational_type_id = $newsFlash->id;
      $informational->subtext = "Warrenpoint scene of heavy flooding - Catastrophic damage anticipated";
      $informational->simulation()->associate($simulation);
      $informational->save();

      $informational = new Informational;
      $informational->text = "Park Flooded!";
      $informational->time_offset = 7200;
      $informational->subtext = "Heavy water logging leaving pitch with long-term damage";
      $informational->simulation()->associate($simulation);
      $informational->informational_type_id = $information->id;
      $informational->save();

        $informational = new Informational;
        $informational->text = "Trending Social Media Post";
        $informational->informational_type_id = $socialMedia->id;
        $informational->author = "@maxExerciseFan";
        $informational->time_offset = 7210;
        $informational->subtext = "Just got out of Clonallon Park in time! #crazyWeather out there!!";
        $informational->metadata = json_encode(['likes' => 51, 'shares' => 129]);
        $informational->simulation()->associate($simulation);
        $informational->save();

        $task = ChallengeTemplate::whereSlug('task')->first();
        $multiChoice = ChallengeTemplate::whereSlug('multichoice')->first();

        Challenge::whereText("Back on the Ball")->delete();
        $challenge = $task->fillChallenge(new Challenge);
        $challenge->simulation()->associate($simulation);
        $challenge->text = "Back on the Ball";
        $challenge->time_offset = 0;
        $challenge->subtext = <<<ENDDESC
        Warrenpoint Park, St Peters GAC and Millburn Park are various elevations and distances from the shoreline. Can you work out roughly how long each is under water for?

        What facilities are at each (e.g. pitches, courts)? How does water affect each of those?
ENDDESC;
        $challenge->save();

        Challenge::whereText("View from Above")->delete();
        $challenge = $multiChoice->fillChallenge(new Challenge);
        $challenge->simulation()->associate($simulation);
        $challenge->text = "Question 1";
        $challenge->time_offset = 4 * 60 * 60;
        $challenge->subtext = <<<ENDDESC
        What is a common storm surge protective measure?
ENDDESC;
        $challenge->options = json_encode([
            ['value' => 'a', 'text' => 'Levee'],
            ['value' => 'b', 'text' => 'Dam'],
            ['value' => 'c', 'text' => 'Sea wall']
        ], true);
        $challenge->correct = '"c"';
        $challenge->save();

        Challenge::whereText("View from Above")->delete();
        $challenge = $task->fillChallenge(new Challenge);
        $challenge->simulation()->associate($simulation);
        $challenge->text = "View from Above";
        $challenge->time_offset = 24 * 60 * 60;
        $challenge->subtext = <<<ENDDESC
        What would Warrenpoint look like from above one day after the flood starts?
ENDDESC;
        $challenge->save();

        $this->attachNearbyFeaturesToSimulation($simulation);

        $this->gridResultFromSimulation($simulation);

        $simulation->status = Simulation::STATUS_COMPLETE;
        $simulation->save();

        $this->command->info('http://localhost:8000/v/#/s/' . $simulation->id);
    }
}

