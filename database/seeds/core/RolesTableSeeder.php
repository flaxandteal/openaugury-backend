<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->delete();
        DB::table('roles')->delete();

        $admin = Role::firstOrCreate(['name' => 'admin']);
        $teacher = Role::firstOrCreate(['name' => 'teacher']);

        $permissions = [
            Permission::firstOrCreate(['name' => 'access panel']),
            Permission::firstOrCreate(['name' => 'access_backend']),
            Permission::firstOrCreate(['name' => 'pick district']),
            Permission::firstOrCreate(['name' => 'view /Role/all']),
            Permission::firstOrCreate(['name' => 'view /Role/edit']),
            Permission::firstOrCreate(['name' => 'view /Permission/all']),
            Permission::firstOrCreate(['name' => 'view /Permission/edit']),
            Permission::firstOrCreate(['name' => 'view /Link/all']),
            Permission::firstOrCreate(['name' => 'view /Link/edit']),
        ];

        foreach ($permissions as $permission) {
            $admin->givePermissionTo($permission);
        }

        $teacher->givePermissionTo('access_backend');

        $adminAndTeacher = [
            Permission::firstOrCreate(['name' => 'view /SimulationTier:Simulation/all']),
            Permission::firstOrCreate(['name' => 'view /SimulationTier:Simulation/edit']),
            Permission::firstOrCreate(['name' => 'view /Interaction:Challenge/all']),
            Permission::firstOrCreate(['name' => 'view /Interaction:Challenge/edit']),
            Permission::firstOrCreate(['name' => 'view /Interaction:Informational/all']),
            Permission::firstOrCreate(['name' => 'view /Interaction:Informational/edit']),
            Permission::firstOrCreate(['name' => 'view /CaseTier:CaseContext/all']),
            Permission::firstOrCreate(['name' => 'view /CaseTier:CaseContext/edit']),
        ];

        foreach ([$admin, $teacher] as $role) {
            foreach ($adminAndTeacher as $permission) {
                $role->givePermissionTo($permission);
            }
        }
    }
}
