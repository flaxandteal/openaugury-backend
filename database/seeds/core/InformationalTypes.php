<?php

use Phaza\LaravelPostgis\Geometries\LineString;
use Phaza\LaravelPostgis\Geometries\Point;
use Phaza\LaravelPostgis\Geometries\Polygon;
use Illuminate\Database\Seeder;

class InformationalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('informational_types')->delete();

      DB::table('informational_types')->insert([
        'name' => 'Information',
        'slug' => 'information'
      ]);

      DB::table('informational_types')->insert([
        'name' => 'Newsflash',
        'slug' => 'newsflash'
      ]);

      DB::table('informational_types')->insert([
        'name' => 'Social Media',
        'slug' => 'social-media'
      ]);
    }
}
