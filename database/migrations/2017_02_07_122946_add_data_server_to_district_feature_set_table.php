<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDataServerToDistrictFeatureSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('district_feature_set', function (Blueprint $table) {
            $table->string('data_server')->default('opendatani');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('district_feature_set', function (Blueprint $table) {
            $table->dropColumn('data_server');
        });
    }
}
