<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSimulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulations', function (Blueprint $table) {
            $table->uuid('id');

            $table->uuid('case_context_id');
            $table->foreign('case_context_id')->references('id')->on('case_contexts')->onDelete('cascade');
            $table->uuid('combination_id');
            $table->foreign('combination_id')->references('id')->on('combinations')->onDelete('cascade');

            $table->json('settings');
            $table->text('definition');
            $table->integer('status');
            $table->point('center')->nullable();
            $table->polygon('extent')->nullable();
            $table->dateTime('begins');
            $table->dateTime('ends');
            $table->json('result');

            $table->uuid('owner_id')->nullable();
            $table->foreign('owner_id')->references('id')->on('users')->onDelete('set null');

            $table->timestamps();
            $table->primary('id');

	    //Necessary columns for Twill
	    $table->dateTime('deleted_at')->nullable();
	    $table->integer('position')->default(0);
	    $table->integer('published')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulations');
    }
}
