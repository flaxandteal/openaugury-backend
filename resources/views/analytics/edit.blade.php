@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Analytic
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($analytic, ['route' => ['analytics.update', $analytic->id], 'method' => 'patch']) !!}

                        @include('analytics.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection