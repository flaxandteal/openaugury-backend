<table class="table table-responsive" id="analytics-table">
    <thead>
        <th>Code</th>
        <th>Name</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($analytics as $analytic)
        <tr>
            <td>{!! $analytic->code !!}</td>
            <td>{!! $analytic->name !!}</td>
            <td>
                {!! Form::open(['route' => ['analytics.destroy', $analytic->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('analytics.show', [$analytic->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('analytics.edit', [$analytic->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>