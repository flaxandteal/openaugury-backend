<table class="table table-responsive" id="featureArcs-table">
    <thead>
        <th>Feature Chunk Id</th>
        <th>Simulation Id</th>
        <th>Arc</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($featureArcs as $featureArc)
        <tr>
            <td>{!! $featureArc->feature_chunk_id !!}</td>
            <td>{!! $featureArc->simulation_id !!}</td>
            <td>{!! $featureArc->arc !!}</td>
            <td>
                {!! Form::open(['route' => ['featureArcs.destroy', $featureArc->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('featureArcs.show', [$featureArc->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('featureArcs.edit', [$featureArc->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>