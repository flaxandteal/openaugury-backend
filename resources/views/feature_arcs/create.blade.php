@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Feature Arc
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'featureArcs.store']) !!}

                        @include('feature_arcs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
