<!-- Cached Data Server Feature Set Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cached_data_server_feature_set_id', 'Cached Data Server Feature Set Id:') !!}
    {!! Form::number('cached_data_server_feature_set_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Feature Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('feature_id', 'Feature Id:') !!}
    {!! Form::text('feature_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<!-- Extent Field -->
<div class="form-group col-sm-6">
    {!! Form::label('extent', 'Extent:') !!}
    {!! Form::text('extent', null, ['class' => 'form-control']) !!}
</div>

<!-- Json Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('json', 'Json:') !!}
    {!! Form::textarea('json', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cachedDataServerFeatures.index') !!}" class="btn btn-default">Cancel</a>
</div>
