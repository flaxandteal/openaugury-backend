@extends('help.template')
@section('content')

<h2>Creating a Target Zone</h2>

<p>In the Admin Panel, within the left-hand
navigation menu, click <em>Target Zones</em>. Use the <em>Add</em> button to
create a new <em>Target Zone</em>, for example, near your school.</p> <p>On the
<em>Target Zone</em> creation page, you will see several fields that can be
filled. Enter a name that will help you find it easily in the <em>Target
Zones</em> list, for example, "near Newcastle" if you wish to simulate a
disaster near Newcastle. This will appear both in the Admin Panel for you to
see, and in the front-end for students to see.</p>

<div class='help-figure'>
    <img alt="Listing Target Zones" src='/images/screenshots/screenshot-as-target-zones-all.png'/>
    <p>Figure: Listing <em>Target Zones</em></p>
</div>

<p>The start time is the
initial time shown on the Timeline when students open the Map Viewer to view
your Simulations for this Target Zone. The end time is the cut-off point on the
Timeline after which any Simulations for this Target Zone will show no change.
For your first attempts, it is recommended that you keep them roughly 10h apart
to obtain best results.</p>

<p>When you have saved your new <em>Target Zone</em>, you will be taken to an
editing page where you can select from
the map features available in that area, which you would like shown when your
students are exploring scenarios. For instance, you may wish to show nearby
Community Centres, but not Leisure Facilities. In this case you should tick
Community Centres, but leave Leisure Facilities unticked. Save the <em>Target
Zone</em>.</p>

<p>You are now ready to create <em>Simulations</em> within your
<em>Target Zone</em>.</p>

<div class='help-figure'>
    <img alt="Creating/editing a Target Zone" src='/images/screenshots/screenshot-as-target-zones-edit.png'/>
    <p>Figure: Creating/editing a <em>Target Zone</em></p>
</div>


@endsection
