@extends('help.template')
@section('content')

<h1>Simulations</h1>

<h3>Models</h3>

<p>Our initial set of simulation models include:</p>

<ul>
    <li>Hurricane storm surges
        <ul>
            <li>elevation-based storm surge model</li>
        </ul>
    </li>
    <li>Earthquakes
        <ul>
            <li>concentric isoseismic contours</li>
            <li>building-code based seismic model</li>
        </ul>
    </li>
    <li>Volcanoes
        <ul>
            <li>(coming soon) concentric ash cloud risk model</li>
            <li>(coming soon) lava flow overland model</li>
        </ul>
    </li>
</ul>

<p>Note that these are all highly approximate, designed to be
illustrative and should not be considered accurate. They should not, in any
circumstances, be used for making predictions in areas of disaster activity, or
for real-life planning.</p>

<p>Our design allows for easy incorporation of new
simulation models, both within the existing set of phenomena and new phenomena
not yet included.</p>

<h4>Hurricane storm surges</h4>

<p>Hurricane storm surges in OurRagingPlanet are based on the most extreme type
of storm surges seen on Earth, where levels can rise 1m/hour over a number of
hours. This was seen, for example, during Hurricane Katrina in the United States.</p>

<p>It is important to reinforce to students that these are of a difference scale of
magnitude, both in speed of onset and peak deviation, to those likely to occur in Western Europe.</p>

<h3>Create a Simulation</h3>

<p>In the Admin Panel,
within the left-hand navigation menu, click <em>Simulations</em>. Use the
<em>Add</em> button to create a new <em>Simulation</em>.</p>

<div class='help-figure'>
    <img alt="Create/edit a Simulation" src='/images/screenshots/screenshot-as-simulations-edit.png'/>
    <p>Create/edit a <em>Simulation</em></p>
</div>

<p>Select the type
of natural disaster you wish to simulate from the Phenomenon drop-down box.
There may be more than one type of simulation for a type of physical phenomenon
- e.g. a simple ring-based earthquake model (which creates concentric contour
rings showing risk), and a more complex earthquake model based on
approximations used in building codes - you may wish to create a Simulation for
each and examine each in the Map Viewer to determine which best suits the
purposes of your lesson.</p>

<p>Next, choose the <em>Target Zone</em> that you
created (above) and click <em>Save</em>. The start and end times you set for
the Target Zone will be used set the period over which the Simulation is
simulated.</p>

<p>This will take you to an editing page, where you can add
<em>Informationals</em> and <em>Tasks</em>. You can do so now, or return to do
so later by selecting your new Simulation from the Simulation index, reached by
clicking the <em>Simulation</em> entry in the left-hand navigation menu.</p>

<h3>Shared Simulations</h3>

Shared Simulations are a resource to help you get up and running quickly, by copying an existing
Simulation to your own account. You can then edit this, add tasks/informationals, re-simulate it
or otherwise modify it as you like without affecting any other teachers. To do, select <em>Shared Simulations</em>
in the left-hand navigation menu and click <em>Copy</em> beside the Simulation you wish to copy.
It will then appear in your list of Simulations.

<div class='help-figure'>
    <img alt="Listing Simulations" src='/images/screenshots/screenshot-as-simulations-my.png'/>
    <p>Listing <em>Simulations</em></p>
</div>

@endsection
