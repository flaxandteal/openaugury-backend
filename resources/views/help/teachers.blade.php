@extends('help.template')
@section('content')

<h1>For teachers</h1>

<h3>Logging in to the Admin Panel</h3>

<p>Go to <a href='https://maps.ourragingplanet.com/panel'>https://maps.ourragingplanet.com/panel</a> in
your browser. Enter your email address and password, then click
<em>Login</em>.</p>

<p>Within the <em>Admin Panel</em>, you can manage <a href='/help/simulations'>Simulations</a>,
<a href='/help/target-zones'>Target Zones</a>, <a href='/help/informationals'>Informationals</a>
and <a href='/help/tasks'>Tasks</a>.

<div class='help-figure'>
    <img alt='Dashboard of the Admin Panel' src='/images/screenshots/screenshot-as-dashboard.png'/>
    <p>Figure: Dashboard of the <em>Admin Panel</em></p>
</div>

@endsection
