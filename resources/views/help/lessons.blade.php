@extends('help.template')
@section('content')

<h1>Lessons</h1>

<p>As part of this
application, we provide sample lessons to help you get started with
OurRagingPlanet.</p>

<ul>
    <li><a href='/downloads/lesson-1-storm-surge.docx'>Lesson 1: Hurricane Storm Surge</a></li>
</ul>

<h3>Educational target</h3>


<p>Students will gain an understanding of how the dynamic nature of the planet affects the human and natural world, in measurable terms to which they can easily relate. They will be able to visually retain an understanding of natural disasters, the need for resilience and the impact on individuals in those contexts.</p>

<p>In a classroom setting, students have set objectives to work through a range of scenarios contributing to age appropriate learning objects. The interface is tailored to simplify use for both students and teachers, to enable quick set-up and minimize in-classroom preparation time. As the resource will be accessible internally and externally, it may be included into both lesson plans and assignments.</p>

<p>At Key Stage 3 level in Geography, the resource contributes to the educational objectives primarily through the Mutual Understanding and Media Awareness components, as well as Communication cross-curricular skills, understanding and relating the point of view of survivors of natural disasters.</p>

<p>Within CCEA's Geography GCSE specification, applications are primarily within Unit 1 Theme A: The Dynamic Landscape and Unit 1 Theme C: The Restless Earth, which provide students with insight into natural disasters, their causes, their consequences and their management.  The geographic mapping element also contributes to the requirements of Unit 2 Theme A: People and Where They Live.</p>

<p>While our target is primarily older students, teachers may find aspects useful at Key Stage 2, particularly for developing basic map-reading skills. Within the context of the "Change over Time" and "Movement and Energy" syllabus sections, the resource may be used to help students engage with the effects and consequences of natural change.</p>

@endsection
