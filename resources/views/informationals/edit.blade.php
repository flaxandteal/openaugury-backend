@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Informational
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($informational, ['route' => ['informationals.update', $informational->id], 'method' => 'patch']) !!}

                        @include('informationals.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection