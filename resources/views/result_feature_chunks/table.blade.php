<table class="table table-responsive" id="resultFeatureChunks-table">
    <thead>
        <th>Chunk</th>
        <th>Simulation Id</th>
        <th>Feature Set Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($resultFeatureChunks as $resultFeatureChunk)
        <tr>
            <td>{!! $resultFeatureChunk->chunk !!}</td>
            <td>{!! $resultFeatureChunk->simulation_id !!}</td>
            <td>{!! $resultFeatureChunk->feature_set_id !!}</td>
            <td>
                {!! Form::open(['route' => ['resultFeatureChunks.destroy', $resultFeatureChunk->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('resultFeatureChunks.show', [$resultFeatureChunk->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('resultFeatureChunks.edit', [$resultFeatureChunk->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>