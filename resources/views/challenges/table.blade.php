<table class="table table-responsive" id="challenges-table">
    <thead>
        <th>Text</th>
        <th>Subtext</th>
        <th>Options</th>
        <th>Correct</th>
        <th>Mark</th>
        <th>Challenge Template Id</th>
        <th>Case Context Id</th>
        <th>Result Analytic Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($challenges as $challenge)
        <tr>
            <td>{!! $challenge->text !!}</td>
            <td>{!! $challenge->subtext !!}</td>
            <td>{!! $challenge->options !!}</td>
            <td>{!! $challenge->correct !!}</td>
            <td>{!! $challenge->mark !!}</td>
            <td>{!! $challenge->challenge_template_id !!}</td>
            <td>{!! $challenge->case_context_id !!}</td>
            <td>{!! $challenge->result_analytic_id !!}</td>
            <td>
                {!! Form::open(['route' => ['challenges.destroy', $challenge->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('challenges.show', [$challenge->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('challenges.edit', [$challenge->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>