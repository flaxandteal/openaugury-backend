<table class="table table-responsive" id="worksheetAnswers-table">
    <thead>
        <th>First</th>
        <th>Final</th>
        <th>Mark</th>
        <th>Worksheet Id</th>
        <th>Challenge Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($worksheetAnswers as $worksheetAnswer)
        <tr>
            <td>{!! $worksheetAnswer->first !!}</td>
            <td>{!! $worksheetAnswer->final !!}</td>
            <td>{!! $worksheetAnswer->mark !!}</td>
            <td>{!! $worksheetAnswer->worksheet_id !!}</td>
            <td>{!! $worksheetAnswer->challenge_id !!}</td>
            <td>
                {!! Form::open(['route' => ['worksheetAnswers.destroy', $worksheetAnswer->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('worksheetAnswers.show', [$worksheetAnswer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('worksheetAnswers.edit', [$worksheetAnswer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>