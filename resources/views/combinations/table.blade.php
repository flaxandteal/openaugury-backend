<table class="table table-responsive" id="combinations-table">
    <thead>
        <th>Status</th>
        <th>Numerical Model Id</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($combinations as $combination)
        <tr>
            <td>{!! $combination->status !!}</td>
            <td>{!! $combination->numerical_model_id !!}</td>
            <td>{!! $combination->owner_id !!}</td>
            <td>
                {!! Form::open(['route' => ['combinations.destroy', $combination->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('combinations.show', [$combination->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('combinations.edit', [$combination->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>