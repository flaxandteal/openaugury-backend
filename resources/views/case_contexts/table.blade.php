<table class="table table-responsive" id="caseContexts-table">
    <thead>
        <th>District Id</th>
        <th>Name</th>
        <th>Center</th>
        <th>Extent</th>
        <th>Begins</th>
        <th>Ends</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($caseContexts as $caseContext)
        <tr>
            <td>{!! $caseContext->district_id !!}</td>
            <td>{!! $caseContext->name !!}</td>
            <td>{!! $caseContext->center !!}</td>
            <td>{!! $caseContext->extent !!}</td>
            <td>{!! $caseContext->begins !!}</td>
            <td>{!! $caseContext->ends !!}</td>
            <td>{!! $caseContext->owner_id !!}</td>
            <td>
                {!! Form::open(['route' => ['caseContexts.destroy', $caseContext->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('caseContexts.show', [$caseContext->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('caseContexts.edit', [$caseContext->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>