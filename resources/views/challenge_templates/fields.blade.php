<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtext Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('subtext', 'Subtext:') !!}
    {!! Form::textarea('subtext', null, ['class' => 'form-control']) !!}
</div>

<!-- Options Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('options', 'Options:') !!}
    {!! Form::textarea('options', null, ['class' => 'form-control']) !!}
</div>

<!-- Correct Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('correct', 'Correct:') !!}
    {!! Form::textarea('correct', null, ['class' => 'form-control']) !!}
</div>

<!-- Mark Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mark', 'Mark:') !!}
    {!! Form::number('mark', null, ['class' => 'form-control']) !!}
</div>

<!-- Analytic Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('analytic_id', 'Analytic Id:') !!}
    {!! Form::number('analytic_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('challengeTemplates.index') !!}" class="btn btn-default">Cancel</a>
</div>
