@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Challenge Template
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($challengeTemplate, ['route' => ['challengeTemplates.update', $challengeTemplate->id], 'method' => 'patch']) !!}

                        @include('challenge_templates.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection