<table class="table table-responsive" id="simulations-table">
    <thead>
        <th>Case Context Id</th>
        <th>Combination Id</th>
        <th>Settings</th>
        <th>Definition</th>
        <th>Status</th>
        <th>Center</th>
        <th>Extent</th>
        <th>Begins</th>
        <th>Ends</th>
        <th>Result</th>
        <th>Owner Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($simulations as $simulation)
        <tr>
            <td>{!! $simulation->case_context_id !!}</td>
            <td>{!! $simulation->combination_id !!}</td>
            <td>{!! $simulation->settings !!}</td>
            <td>{!! $simulation->definition !!}</td>
            <td>{!! $simulation->status !!}</td>
            <td>{!! $simulation->center !!}</td>
            <td>{!! $simulation->extent !!}</td>
            <td>{!! $simulation->begins !!}</td>
            <td>{!! $simulation->ends !!}</td>
            <td>{!! $simulation->result !!}</td>
            <td>{!! $simulation->owner_id !!}</td>
            <td>
                {!! Form::open(['route' => ['simulations.destroy', $simulation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('simulations.show', [$simulation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('simulations.edit', [$simulation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>