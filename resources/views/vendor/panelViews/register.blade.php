@extends('panelViews::master')
@section('bodyClass')
register
@stop
@section('body')
    <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    
                    <div class="login-panel panel panel-default animated fadeInDown">
                     
                        <div class="panel-body">
                            <div class="logo-holder">
                                <img src="{{asset(Config::get('panel.logo'))}}" />
                            </div>
                            {!! Form::open(array('url' => 'register')) !!}
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.email') }}" name="email" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="{{ \Lang::get('panel::fields.password') }}" name="password" type="password" value="">
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input type="submit"  class="btn btn-lg btn-success btn-block" value="Register">
                                </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
    </div>
@stop

