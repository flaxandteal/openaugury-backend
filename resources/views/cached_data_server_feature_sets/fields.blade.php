<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Owner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner', 'Owner:') !!}
    {!! Form::text('owner', null, ['class' => 'form-control']) !!}
</div>

<!-- License Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('license_title', 'License Title:') !!}
    {!! Form::text('license_title', null, ['class' => 'form-control']) !!}
</div>

<!-- License Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('license_url', 'License Url:') !!}
    {!! Form::text('license_url', null, ['class' => 'form-control']) !!}
</div>

<!-- Uri Field -->
<div class="form-group col-sm-6">
    {!! Form::label('uri', 'Uri:') !!}
    {!! Form::text('uri', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Server Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_server', 'Data Server:') !!}
    {!! Form::text('data_server', null, ['class' => 'form-control']) !!}
</div>

<!-- Data Server Set Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data_server_set_id', 'Data Server Set Id:') !!}
    {!! Form::text('data_server_set_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cachedDataServerFeatureSets.index') !!}" class="btn btn-default">Cancel</a>
</div>
