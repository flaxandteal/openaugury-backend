<?php

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\DataArraySerializer;

trait ApiTestTrait
{
    public function assertApiExactResponse($actualData)
    {
        $this->assertApiSuccess();

        $response = json_decode($this->response->getContent(), true);
        $this->assertEquals($actualData, $response);
    }

    public function assertApiResponse(Array $actualData)
    {
        $this->assertApiSuccess();

        $response = json_decode($this->response->getContent(), true);
        $responseData = $response['data'];

        $this->assertNotEmpty($responseData[$this->getIdentifier()]);
        $this->assertModelData($actualData, $responseData);
    }

    public function assertApiSuccess()
    {
        $this->assertResponseOk();
    }

    public function reduceToJsonFields(Array $data)
    {
        $jsonFields = $this->getJsonFields();

        if ($jsonFields) {
            $data = array_intersect_key($data, array_flip($jsonFields));
        }

        return $data;
    }

    public function assertModelData(Array $expectedData, Array $actualData)
    {
        $attributes = $actualData['attributes'];

        foreach ($expectedData as $key => $value) {
            $this->assertEquals($value, $attributes[$key]);
        }
    }
}
